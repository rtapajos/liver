#' ---
#' title: We R Live 06: Relacionando dados climáticos com dados de biodiversidade
#' author: mauricio vancine, felipe barros
#' date: 2020-06-09
#' ---

# preparate r -------------------------------------------------------------
# packages
library(geobr) # vetor do brasil
library(raster) # raster
library(sf) # vetor
library(tidyverse) # varios pacotes
library(patchwork) # combina graficos

# download data -----------------------------------------------------------
# worldclim
# directory
dir.create("data"); setwd("data")
getwd()

# download
download.file(url = "https://biogeo.ucdavis.edu/data/worldclim/v2.1/base/wc2.1_10m_bio.zip",
              destfile = "wc2.1_10m_bio.zip", mode = "wb")

# unzip
unzip(zipfile = "wc2.1_10m_bio.zip")

# data paper
download.file(url = "https://esajournals.onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1002%2Fecy.2785&file=ecy2785-sup-0001-DataS1.zip",
              destfile = "ecy2785-sup-0001-DataS1.zip", mode = "wb")

# unzip
unzip(zipfile = "ecy2785-sup-0001-DataS1.zip")

# import data -------------------------------------------------------------
# worldclim
# import
var <- dir(pattern = ".tif$") %>% 
  raster::stack()
var

# rename
names(var)
names(var) <- c("bio01", paste0("bio", 10:19), paste0("bio0", 2:9))
names(var)
var

# map
var$bio01

var$bio01 %>% 
  raster::rasterToPoints() %>% 
  head()

var$bio01 %>% 
  raster::rasterToPoints() %>% 
  tibble::as_tibble() %>% 
  head()

var$bio01 %>% 
  raster::rasterToPoints() %>% 
  tibble::as_tibble() %>% 
  ggplot() + 
  aes(x = x, y = y, fill = bio01) +
  geom_raster() +
  scale_fill_viridis_c() +
  # coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", fill = "BIO01")

# atlantic forest
af <- geobr::read_biomes(year = 2004) %>% 
  dplyr::filter(name_biome == "Mata Atlântica") %>% 
  sf::st_crop(xmin = -56, ymin = -30, xmax = -34.5, ymax = -5) %>% 
  sf::st_transform(crs = 4326)
af

ggplot() + 
  geom_sf(data = af) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude")

# data paper
da <- readr::read_csv("ATLANTIC_MAMMAL_MID_LARGE _assemblages_and_sites.csv")
da

dplyr::glimpse(da) # analise preliminar dos dados

da$Latitude %>% table(useNA = "always") # explicar table - gera tabela de frequencia

# prepare data
da_clean <- da %>% 
  tidyr::drop_na(Longitude, Latitude) %>% # retirar nas
  dplyr::filter(Latitude != "#REF!") %>%  # filtrar linhas
  dplyr::mutate(Latitude = as.numeric(Latitude)) %>% # add coluna convertendo para numerico
  dplyr::distinct(ID, Species_name_on_paper, .keep_all = TRUE) %>% 
  dplyr::add_count(ID, name = "n_species") %>% # numero de spp
  dplyr::distinct(ID, .keep_all = TRUE)
dplyr::glimpse(da_clean)

# convert coordinates do vector -------------------------------------------
da_clean_vector <- da_clean %>% 
  dplyr::mutate(lon = Longitude, lat = Latitude) %>% 
  sf::st_as_sf(coords = c("lon", "lat"), crs = 4326)
da_clean_vector

# map
ggplot() + 
  geom_sf(data = af) +
  geom_sf(data = da_clean_vector  %>% dplyr::distinct(ID, .keep_all = TRUE), 
          aes(size = n_species), color = "black", 
          fill = "purple", pch = 21, alpha = .5) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", size = "Número de espécies") +
  theme(legend.position = c(.7, .2))

# intersection - apenas os internos ao limite do bioma
da_clean_vector_af <- sf::st_intersection(da_clean_vector, af)
da_clean_vector_af

# export
readr::write_csv(da_clean_vector_af, "atlantic_mammals.csv")

# map
ggplot() + 
  geom_sf(data = af) +
  geom_sf(data = da_clean_vector_af  %>% dplyr::distinct(ID, .keep_all = TRUE), 
          aes(size = n_species), color = "black", 
          fill = "purple", pch = 21, alpha = .5) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", size = "Número de espécies") +
  theme(legend.position = c(.7, .2))

# crop e mask -------------------------------------------------------------
# adjust to atlantic forest biome - extension
var_crop_af <- raster::crop(var, af)
var_crop_af
plot(var_crop_af$bio01)

# adjust to atlantic forest biome - mask
var_mask_af <- raster::mask(var, af)
var_mask_af
plot(var_mask_af$bio01)

# adjust to atlantic forest biome - extension and mask
var_crop_mask_af <- raster::crop(var, af) %>% raster::mask(., af)
var_crop_mask_af
plot(var_crop_mask_af$bio01)

# map
var_crop_mask_af$bio01 %>% 
  raster::rasterToPoints() %>% 
  tibble::as_tibble() %>% 
  ggplot() + 
  aes(x = x, y = y, fill = bio01) +
  geom_raster() +
  scale_fill_viridis_c() +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", fill = "BIO01")

# extract values from raster to points ------------------------------------
# extract and bind cols
da_clean_vector_af_bio <- raster::extract(var, da_clean_vector_af) %>% 
  tibble::as_tibble() %>% 
  dplyr::bind_cols(da_clean_vector_af, .)
da_clean_vector_af_bio

# statistics --------------------------------------------------------------
# remove duplicate
da_clean_vector_af_bio_unique <- da_clean_vector_af_bio %>% 
  dplyr::distinct(ID, .keep_all = TRUE)
da_clean_vector_af_bio_unique

# histogram
hist <- da_clean_vector_af_bio_unique %>% 
  dplyr::select(bio01) %>% 
  ggplot() +
  aes(x = bio01) +
  geom_histogram(color = "white", bins = 30, fill = cptcity::cpt(pal = cptcity::find_cpt("temperature")[2], n = 30)) +
  theme_minimal() +
  labs(x = "BIO01", y = "Frequência Absoluta")
hist

map <- var_crop_mask_af$bio01 %>% 
  raster::rasterToPoints() %>% 
  tibble::as_tibble() %>% 
  ggplot() + 
  aes(x = x, y = y, fill = bio01) +
  geom_raster() +
  scale_fill_gradientn(colors = cptcity::cpt(pal = cptcity::find_cpt("temperature")[2], n = 30)) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", fill = "BIO01")
map

# combine
map + hist
map / hist

# histograms with facet ---------------------------------------------------
# wide to long
da_clean_vector_af_bio_unique_long <- da_clean_vector_af_bio_unique %>% 
  sf::st_drop_geometry() %>% 
  dplyr::select(ID, contains("bio0"), contains("bio1")) %>% 
  tidyr::pivot_longer(cols = -ID, names_to = "bios", values_to = "values")
da_clean_vector_af_bio_unique_long

# histogram
hist_facet <- da_clean_vector_af_bio_unique_long %>% 
  ggplot() +
  aes(x = values) +
  geom_histogram(color = "white", bins = 30, fill = rep(cptcity::cpt(pal = cptcity::find_cpt("temperature")[2], n = 30), 19)) +
  facet_wrap(vars(bios), scales = "free") +
  theme_minimal() +
  labs(x = "Valores", y = "Frequência Absoluta")
hist_facet

# regression with facet --------------------------------------------------------------
# select id and species number
is_sn <- da_clean_vector_af_bio_unique %>% 
  sf::st_drop_geometry() %>% 
  dplyr::select(ID, n_species)
is_sn

# wide to long
da_clean_vector_af_bio_unique_long_species_number <- da_clean_vector_af_bio_unique_long %>% 
  dplyr::left_join(is_sn, by = "ID")
da_clean_vector_af_bio_unique_long_species_number

# regression
reg_facet <- da_clean_vector_af_bio_unique_long_species_number %>% 
  ggplot() +
  aes(x = values, y = n_species) +
  geom_point(pch = 21, fill = "gray") +
  stat_smooth(color = "red", method = "gam", formula = y ~ s(x, bs = "cs"), method.args = list(family = "poisson")) +
  facet_wrap(vars(bios), scales = "free") +
  theme_bw() +
  labs(x = "Valores", y = "Número de espécies")
reg_facet

# end ---------------------------------------------------------------------