---
title: "We R Live 10: Join - como associar dados à vetores<br><br>"
subtitle: "<br>GeoCast Brasil" 
author: "Maurício Vancine <br> Felipe Sodré M. Barros <br>"
date: "<br> 14/07/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 10

## Tópicos
### <u><b>Introdução (20 min.)</b></u>
#### 1 Desafio da We R Live 10
#### 2 Pacotes a serem usados
#### 3 Considerações conceituais

--

### <u><b>Mão na massa (30/40 min.)</b></u>
#### 4 Carregar, visualizar e manejar dados vetoriais
#### 5 Join tabular e espacial
--

### <u><b> Considerações finais (5 min)</u></b>

---

class: inverse, middle, center

# Mas antes! Recados!!!

---

# Recados

<br>

### 1. Apoie as iniciativas do GeoCast Brasil:
- Não deixe de curtir as *lives* e videos;
- Não deixe de se inscrever no canal;
- Ajude divulgando nas redes sociais;

### 2. Lives passadas: visite nosso site:
- Site: https://werlive.netlify.app/

### 3. Dúvidas e sugestões: issues
- GitLab: https://gitlab.com/geocastbrasil/liver/-/issues

<br>
.center[
[`r icon::fa_gitlab(size = 2)`](https://gitlab.com/geocastbrasil/liver)
[`r icon::fa_twitter(size = 2)`](https://twitter.com/GeoCastBrasil)
[`r icon::fa_instagram(size = 2)`](https://www.instagram.com/geocastbrasil/)
[`r icon::fa_facebook(size = 2)`](https://facebook.com/GeoCastBrasil/)]

---

# Recados

## Estamos com um grupo aberto no Telegram!

<br><br><br><br><br>

.center[
[`r icon::fa_telegram(size = 5)`](https://t.me/GeoCastBrasil)]

---

background-image: url(img/geoind.png),url(img/felipe.png)
background-size: 800px,100px
background-position: 30% 35%,90% 85%

# Recados

## Geo Independência 

### We R Live: R para análise espacial

<br><br><br><br><br><br><br><br><br><br><br><br>

https://geoind.wordpress.com/2020/07/06/r-para-analise-espacial/

---

class: inverse, middle, center

# 1 Desafio da We R Live 10

---

background-image: url(img/package_tidyverse.png),url(img/package_geobr.png),url(img/package_sf.gif)
background-size: 150px,150px,170px
background-position: 40% 90%,65% 90%,90% 90%

# 1 Desafio da We R Live 10

### Manejar dados vetoriais e Join tabular e espacial
- Usar funções do *tidyverse* para:
  - selecionar feições de vetores
  - resumir dados de vetores
  - criar colunas em vetores
- Realizar *joins* tabulares e espaciais

--

### Extras: 
- Mapear dados de COVID-19 para o Estado do Rio de Janeiro
- Revisão de *tidyverse*: conceitos e pacotes

---

class: inverse, middle, center

# 2 Pacotes a serem usados

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 85% 95%

# 2 Pacotes a serem usados

### **`sf`**
https://r-spatial.github.io/sf/index.html

### **`tidyverse`** - tibble, magritter, tidyr, dplyr
https://www.tidyverse.org/

### **`geobr`**
https://github.com/ipeaGIT/geobr

### **`tmap`**
https://github.com/mtennekes/tmap

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 50% 95%

# 2 Pacotes a serem usados

## Instalação

### Instalar pacotes
```{r eval=FALSE}
install.packages(c("sf", "tidyverse", "geobr", "tmap"), 
                 dependencies = TRUE)
```

---

class: inverse, middle, center

# 3 Considerações conceituais

---

background-image: url(img/wesley.jpg)
background-size: 350px
background-position: 50% 70%

# 3 Considerações conceituais

## Dados de COVID-19 para o Brasil
Todos os dados usados aqui foram coletados do [site](https://covid19br.wcota.me/) do pesquisador [Wesley Cota](https://wesleycota.com/), que mostra o número de casos confirmados e mortes de COVID-19

<br><br><br><br><br><br><br><br><br><br><br><br>

.center[Wesley Cota]

---

class: inverse, middle, center

# Mas antes, uma pequena revisão de *tidyverse*

---

class: clear, center, middle
background-image: url(img/package_tidyverse.png)
background-size: 400px
background-position: 50% 50%

---

# tidyverse

### O tidyverse é um **conjunto de pacotes** designados para **Data Science** (mas não só)

<br>

--

### Todos os pacotes compartilham uma **filosofia** de design, gramática e estruturas de dados

<br>

--

### É um **"dialeto"** novo para a linguagem R

<br>

--

### **tidy**: organizado, arrumado, ordenado

### **verse**: universo

---

background-image: url(img/tidyverse_packages.png)
background-size: 650px
background-position: 50% 50%

# tidyverse

## Pacotes

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [Teaching the Tidyverse in 2020 - Part 1: Getting Started](https://education.rstudio.com/blog/2020/07/teaching-the-tidyverse-in-2020-part-1-getting-started/)

---

background-image: url(img/tidy_papers.png)
background-size: 350px
background-position: 80% 95%

# tidyverse

## Artigos
> - Wickham, Hadley. ["Tidy data."](https://www.jstatsoft.org/article/view/v059i10) Journal of Statistical Software 59.10 (2014): 1-23.

> - Wickham, Hadley, et al. ["Welcome to the Tidyverse."](https://joss.theoj.org/papers/10.21105/joss.01686) Journal of Open Source Software 4.43 (2019): 1686.

---

# tidyverse

### Para utilizar os pacotes do **tidyverse** é preciso instalar e carregar o pacote `tidyverse`

```{r,eval=FALSE}
# instalar o pacote
install.packages("tidyverse")
```

--

```{r,message=TRUE}
# carregar o pacote
library(tidyverse)
```

---

background-image: url(img/code_snake_case.png)
background-size: 500px
background-position: 80% 80%

# tidyverse

## IMPORTANTE!

### Todas as funções dos pacotes atrelados ao **tidyverse** usam `_` (underscore) para separar os nomes internos das funções (snake_code)

<br><br>

`read_csv()`

`read_xlsx()`

`as_tibble()`

`left_join()`

`group_by()`

---

class: clear, center, middle
background-image: url(img/package_magrittr.png)
background-size: 400px
background-position: 50% 50%

---

# magrittr (pipe - %>%)

### Atalho: `crtl + shift + M`
```{r}
# sem pipe
sqrt(sum(1:100))
```

--

```{r}
# com pipe
1:100 %>% 
  sum() %>% 
  sqrt()
```

---

class: clear, inverse, center, middle
background-image: url(img/gif_mario.gif)
background-size: 700px
background-position: 50% 50%

---

class: clear, center, middle
background-image: url(img/package_readr.png)
background-size: 400px
background-position: 50% 50%

---

class: clear, center, middle
background-image: url(img/package_tibble.png)
background-size: 400px
background-position: 50% 50%

---

class: clear, center, middle
background-image: url(img/package_tidyr.png)
background-size: 400px
background-position: 50% 50%

---

background-image: url(img/tidyr_data.png)
background-size: 800px
background-position: 50% 85%

# tidyr

## Nos dados tidy:
### 1. Cada variável em uma coluna
### 2. Cada observação em uma linha
### 3. Cada valor como uma célula

---

class: clear, center, middle
background-image: url(img/package_dplyr.png)
background-size: 400px
background-position: 50% 50%

---

class: clear, center, middle
background-image: url(img/tidyr_rpg.jpeg)
background-size: 850px
background-position: 50% 30%

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Autora: [@ViviFabrien](https://twitter.com/ViviFabrien)

---

# dplyr

<br><br>

### O **dplyr** é um pacote que **facilita** o trabalho de manipulação de dados (filtragem, reordenamento, seleção, etc)

--

### Ele foi construído com o intuito de obter uma forma **mais rápida** e **expressiva** de tratar os dados 

--

### O **tibble** é a **versão de data frame** mais **conveniente** para **se usar** com dplyr

---

background-image: url(img/dplyr_manipulations.png)
background-size: 600px
background-position: 50% 70%

# dplyr

### **Funções verbais**

#### **1. mutate()**: adiciona novas colunas ou resultados em colunas existentes
#### **2. filter()**: seleciona linhas com base em valores
#### **3. summarise()**: agrega ou resume os dados através de funções

<br><br><br><br><br><br><br><br><br><br><br>

Fonte: [Jeff Griesemer](https://towardsdatascience.com/data-manipulation-in-r-with-dplyr-3095e0867f75)

---

background-image: url(img/dplyr_join.png)
background-size: 350px
background-position: 50% 60%

# dplyr

### **Funções verbais**

#### **4. _join()**: junta dados de duas tabelas através de uma coluna chave

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://rpubs.com/williamsurles/293454

---

background-image: url(img/dplyr_leftjoin.png), url(img/dplyr_rightjoin.png), url(img/dplyr_innerjoin.png), url(img/dplyr_fulljoin.png)
background-size: 600px, 600px, 600px, 600px
background-position: 50% 20%, 50% 45%, 50% 70%, 50% 95%

# dplyr

## *_join

---

background-image: url(img/dplyr_semijoin.png), url(img/dplyr_antijoin.png)
background-size: 700px, 700px
background-position: 50% 40%, 50% 80%

# dplyr

## *_join

---

class: clear, center, middle
background-image: url(img/package_tidyverse.png),url(img/package_sf.gif)
background-size: 350px,400px
background-position: 10% 50%,90% 50%

---

# dplyr

## 1. Operações de dados de atributo
- Criar atributos e remover informação espacial
- Subconjunto
- Agregação
- Junção de dados tabulares à dados espaciais

--

## 2. Operações de dados espaciais
- Junção espacial - junção de dois ou mais vetores

--

background-image: url(img/spatial_join.png)
background-size: 500px
background-position: 50% 97%


---

background-image: url(img/cover_geocompr.png)
background-size: 280px
background-position: 90% 55%

# Mais informações

## Geocomputation with R (2019)

- Cap. 3 Attribute data operations
- 3.2 Vector attribute manipulation
- 3.2.1 Vector attribute subsetting
- 3.2.2 Vector attribute aggregation
- 3.2.3 Vector attribute joining
- 3.2.4 Creating attributes and <br>
removing spatial information


- Cap. 4 Spatial data operations
- 4.2 Spatial operations on vector data
- 4.2.3 Spatial joining

<br><br><br>

[*] https://geocompr.robinlovelace.net/

---

class: inverse, middle, center

# Mão na massa!!! `r icon::fa_laptop_code()`

---

# Script para essa live

<br><br><br><br><br><br><br>

### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive10/werlive10.R`]

---

class: inverse, middle, center

# Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

---

class: inverse, center
background-image: url(img/meme.jpg)
background-size: 500px

# Não seja essa pessoa...

---

background-image: url(img/igor.jpg),url(img/package_florestal.png),url(img/package_florestal2.jpg)
background-size: 150px,200px,400px
background-position: 5% 73%,20% 40%,70% 70%

# Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

## Próxima Live (21/07): 

### We R Live 11: *florestal* - Resultados para Inventários Florestais

<br><br><br><br><br><br><br><br><br><br>

Igor Cobelo

https://cran.r-project.org/web/packages/florestal/index.html

---

class: clear

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [@mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)