---
title: "We R Live 08: Introdução à Estatística Espacial II<br><br>"
subtitle: "<br>GeoCast Brasil" 
author: "Felipe Sodré M. Barros<br> Maurício Vancine <br> "
date: "<br> 23/06/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 08

## Tópicos
### <u><b>Introdução (40 min.)</b></u>
#### 1 Desafio da We R Live 08
#### 2 Correções da live anterior
#### 3 Pacotes a serem usados
#### 4 Considerações conceituais  

  * 4.1 Revisão análise de primeira ordem
  * 4.2 Análise de segunda ordem

### <u><b>Mão na massa (30/40 min.)</b></u>

### <u><b> Considerações finais (5 min)</u></b>

---

class: inverse, middle, center

# Mas antes, recados!!!

---

# Recados

<br>

### 1. Apoie as iniciativas do GeoCast Brasil:
- Não deixe de curtir as *lives* e vídeos;
- Não deixe de se inscrever no canal;
- Ajude divulgando nas redes sociais;

### 2. Lives passadas, visite nosso site:
- Site: https://werlive.netlify.app/

### 3. Dúvidas e sugestões: issues
- GitLab: https://gitlab.com/geocastbrasil/liver/-/issues

<br>
.center[
[`r icon::fa_twitter(size = 2)`](https://twitter.com/GeoCastBrasil)
[`r icon::fa_gitlab(size = 2)`](https://gitlab.com/geocastbrasil/liver)
[`r icon::fa_instagram(size = 2)`](https://www.instagram.com/geocastbrasil/)
[`r icon::fa_facebook(size = 2)`](https://facebook.com/GeoCastBrasil/)
]

---

class: inverse, middle, center

# 1 Desafio da We R Live 08

---

# 1 Desafio da We R Live 08

### Introdução Estatítica Espacial
- Introdução à análise de processos pontuais II
  - Análise de primeira ordem `r icon::fa_check()`
  - Análise de segunda ordem

### Extras
- Correlação espacial  
(Estrutura da distribuição)

--

background-image: url(img/livro_rstats.jpeg)
background-size: 250px
background-position: 80% 95%

---

background-image: url(img/Noticia_Violencia.png)
background-size: 550px
background-position: 50% 70%

# 1 Desafio da We R Live 08
## Estudo de caso
### E mais!
---

class: inverse, middle, center

# 2 Correções da live anterior

---
background-image: url(./img/sp_deprecated.png)
background-size: 400px
background-position: 0% 50%
# 2 Correções da live anterior

## Sim, sp foi descontinuado...

---
background-image: url(./img/sp_deprecated.png), url(./img/sp_deprecatedII.jpg)
background-size: 400px, 500px
background-position: 0% 50%, 100% 50%
# 2 Correções da live anterior

## Sim, sp foi descontinuado...

---

class: inverse, middle, center

# 3 Pacotes a serem usados

---
background-image: url(./img/package_r.png)
background-size: 150px
background-position: 95% 10%

# 3 Pacotes a serem usados

#### **`rgdal`**: Bindings for the 'Geospatial' Data Abstraction Library

#### ~~[**`sp`**](https://edzer.github.io/sp/)~~ `r icon::fa_exclamation_triangle()` DESCONTINUADO!

#### **`maptools`**: [Tools for Handling Spatial Objects](http://r-forge.r-project.org/projects/maptools/)

#### **`raster`**

#### **`sf`**

#### [**`spatstat`**](https://cran.r-project.org/web/packages/spatstat/index.html)


### Instalar pacotes
```{r eval=FALSE}
install.packages( c("spatstat", "rgdal",
                    "maptools", "raster", "sf"), 
                 dependencies = TRUE)
```
---

background-image: url(./img/package_r.png), url(./img/logo_fogo_cruzado_v2.png)
background-size: 150px, 250px
background-position: 95% 10%, 5% 65%

# 3 [outros] Pacotes a serem usados

### **`readr`**

### **`dplyr`**

### **`geobr`**

### **`tmap`**

### [**`crossfire`**](https://fogocruzado.org.br/)
<br><br>
```{r eval=FALSE}
install.packages( c("readr", "dplyr", "geobr", "tmap"), 
                 dependencies = TRUE)
```
---

class: inverse, middle, center

# 4 Considerações conceituais
<br><br>
# 4.1 Revisão análise de primeira ordem

---

# 4 Considerações conceituais

## 4.1 Revisão análise de primeira ordem

### Processos pontuais

Processos pontuais -> Processo estocástico onde temos eventos observados em algumas localidades de uma região, mapeados como ponto: $(x,y)$.

--

Alguma varável pode ser incorporada às observações (altitude, distância do quartel, idade da pessoa doente): $(x, y, z)$.

--

Dentre os objetivos, busca-se identificar a exitência de um padrão de distribuiução espacial a partir dos eventos observados. 

--

Esse padrão de distribuição espacial pode ser uma informação útil para entender influência de outros fatores no desenvolvimento do processo estudado.

---
# 4  Considerações conceituais

## 4.1 Revisão análise de primeira ordem

### **Hipótese nula**

**Modelo conceitual: Complete Spatial Randomness - CSR**

**Homogeneous Poisson Point Process (HPP):**
- Intensidade/Densidade de eventos é constante na região de estudo;
    $\lambda(x) = \lambda > 0$
- Todos os eventos são independentes;

**HPP = CSR**

--

`r icon::fa_exclamation_triangle()` Na maioria dos casos, o pressuposto de **HPP** não é válido.

**Inhomogeneous Poisson Point Process (IPP)**
- Independência entre eventos, mas;
- Intensidade varia espacialmente;

---
# 4  Considerações conceituais

## 4.1 Revisão análise de primeira ordem

Também considerada como análises globais ou de larga escala: analisam a variação do valor médio de eventos no espaço - **intensidade** e a **densidade** de evento.

$\hat\lambda = \frac{n}{a}$

--

background-image: url(./img/kernel.png)
background-size: 700px
background-position: 60% 60%

--

background-image: url(./img/kernel_fig.png)
background-size: 600px
background-position: 60% 60%
<br><br><br><br><br><br><br><br><br>
Fonte: [geografia.ufes.br](http://www.geo.ufes.br/sites/geografia.ufes.br/files/field/anexo/m_bergamasch.pdf)

---
# 4  Considerações conceituais

## 4.1 Revisão análise de primeira ordem

Também considerada como análises globais ou de larga escala: analisam a variação do valor médio de eventos no espaço - **intensidade** e a **densidade** de evento.

$\hat\lambda = \frac{n}{a}$

--

`r icon::fa_lightbulb()` **Resumo: Analisa como a distribuição de determinado processo varia na área de estudo.**

--

`r icon::fa_exclamation_triangle()` Não se está analisando a **estrutura** de distribuição (aglomerado/inibição) do processo.

---

class: inverse, middle, center

# 4 Considerações conceituais
<br><br>
# 4.2 Análise de segunda ordem

---

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

São análises que **baseiam-se nas distâncias** para entender a **estrutura** de distribuição do processo estudado.

São também chamadas de análises locais ou de pequena escala [Bailey e Gatrel (1995), Câmara e Carvalho (2005) e Waller e Gotway (2004)].

--

Câmara e Carvalho (2005) e Waller e Gotway (2004) consideram as análises de
segunda ordem como estimadores de dependência espacial.

--

background-image: url(./img/1st_2nd_order_property.png)
background-size: 500px
background-position: 50% 100%
<br><br><br><br><br><br><br><br>
[Fonte](https://mgimond.github.io/Spatial/point-pattern-analysis.html#distance-based-analysis)

---

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

As distâncias analisadas podem ser:
1. distância ao evento mais próximo (função G);
2. distância entre localidades e eventos (função F);
3. distâncias entre todos os eventos (função K ou Função L);

--

background-image: url(./img/funcao_g.png)
background-size: 375px
background-position: 5% 88%

--

background-image: url(./img/funcao_f.png)
background-size: 375px
background-position: 50% 90%

--

background-image: url(./img/funcao_k.png)
background-size: 650px
background-position: 90% 75%

<br><br><br><br><br><br><br><br><br><br>

[Fonte](https://mgimond.github.io/Spatial/point-pattern-analysis.html#distance-based-analysis)

---

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

### Função de K

Dixon(2002), Péssier e Goreaud (2001): a função K tem a vantagem de possibilitar a compreensão da estrutura espacial do processo em diferentes escalas simultaneamente, o que não é possível com as demais funções. 

Além disso, Bailey e Gatrel (1995) e Dixon (2002): ferramenta para análise exploratória, teste de hipótese, estimação de parâmetros e ajuste de modelos de distribuição espacial para processos pontuais.

---

background-image: url(./img/funcao_g.png), url(./img/funcao_f.png), url(./img/funcao_kII.png)
background-size: 325px,  325px,  265px
background-position: 3% 96%, 58% 98%, 100% 90%

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

---

background-image: url(./img/funcao_g.png), url(./img/funcao_f.png), url(./img/funcao_kII.png), url(./img/Padroes.png)
background-size: 325px,  325px,  265px, 500px
background-position: 3% 96%, 58% 98%, 100% 90%, 50% 20%

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

---

background-image: url(./img/funcao_k_responde.png)
background-size: 800px
background-position: 50% 50%

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
Fonte: Eu mesmo `r icon::fa_grimace()`

---

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

### Hipótese nula

**CSR**

Mas com esse tipo de análise podemos não apenas refutar a $h_{0}$, mas também identificar a estrutura do processo pontual analisado:

1. completamente aleatória (CSR);
2. distribuições regulares (que apresentam um caráter de inibição entre eventos);
3. e as distribuições aglomeradas (que apresentam dependência espacial e interação positiva entre eventos);

---

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

### Funcao K transformada

Propõe-se uma pequena mudança na função K para estabilizar a variância de seus resultados, sendo por isto conhecida como função K transformada ou função L.

Quando `L() < 0` indica que há poucos eventos vizinhos dentro do raio r (evidenciando tendência de inibição entre os eventos - distribuição regular);

Para `L () > 0` , evidencia-se a tendência de interação entre os eventos (distribuição aglomerada para a distância r).

---

# 4  Considerações conceituais

## 4.2 Análise de segunda ordem

### Níveis de significância

Uma forma de identificar o nível de significância das análises de segunda ordem é simulando `m` distribuições espaciais baseadas na ( $h_{0}$ ).

Este método calcula o valor da função K ou L do processo observado para posteriormente simular a mesma função `m` vezes, respeitando a intensidade $\lambda()$ do processo observado, mas segundo o modelo teórico da hipótese nula.

---
background-image: url(./img/plot_leitura.png)
background-size: 800px
background-position: 50% 50%

# 4  Considerações conceituais

---

class: inverse, middle, center

# Referencias!!! `r icon::fa_book_open()`

---
# Referencias

`r icon::fa_book_reader()`   **Todo o conteúdo exposto e muito mais está baseado no Trabalho de Conclusão de Curso do Felipe**. O mesmo se encontra disponível [aqui](https://gitlab.com/geocastbrasil/liver/-/blob/master/static/docs/Barros_FELIPE_ANALISE_PROCESSOS_PONTUAIS.pdf).

BAILEY, Trevor C. e GATRELL, Antony C.. Introductory Methods for Point Patterns. In: Interactive Spatial Data Analysis. 1° Edição. Harlow, Inglaterra. Prentice Hall; 1995

CÂMARA, G.; CARVALHO, M.S.; Análise Espacial de Eventos Pontuais. In: Análise Espacial de Dados Geográficos. 1° Edição. São Paulo, Brasil. INPE; 2005

DIXON, Philip M. Ripley's K function. In: Encyclopedia of Environmetrics. Chichester, Inglaterra.2002. P. 1796-1803

PÉSSIER, R.; GOREAUD, F.;A practical approach to the study of spatial structure in simple cases of heterogeneous vegetation. In: Journal of Vegetation Science. Suecia.2001. P. 99-108

WALLER, Lance A. e GOTWAY Carol A.. Analysis of spatial point process. In: Applied Spatial Statistics for Public Health Data.Edição. Georgia, EUA. Wiley; 2004

---

class: inverse, middle, center

# Mão na massa!!! `r icon::fa_laptop_code()`

---

# Script para essa live

<br><br><br><br><br><br>

### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive08/werlive08.R`]

---

class: inverse, middle, center

# 6 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

---

class: clear
background-image: url(img/logo_werlive.png)
background-size: 400px
background-position: 90% 60%

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [@mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)
