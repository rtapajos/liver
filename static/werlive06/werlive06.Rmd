---
title: "We R Live 06: Extraindo dados climáticos para pontos<br><br>"
subtitle: "<br>GeoCast Brasil" 
author: "Maurício Vancine <br> Felipe Sodré M. Barros <br>"
date: "<br> 09/06/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 06

## Tópicos
### <u><b>Introdução (20 min.)</b></u>
#### 1 Desafio da We R Live 06
#### 2 Pacotes a serem usados
#### 3 Considerações conceituais

--

### <u><b>Mão na massa (30/40 min.)</b></u>
#### 4 Carregar, visualizar e manejar dados climáticos (raster)
#### 5 Extrair valores para pontos
#### 6 Análises estatísticas simples
--

### <u><b> Considerações finais (5 min)</u></b>

---

class: inverse, middle, center

# Mas antes! Recados!!!

---

# Recados

<br><br><br><br><br>

### 1. Lives passadas: visite nosso site (mais informações)
- Site: https://werlive.netlify.app/

### 2. Dúvidas e sugestões: issues
- GitLab: https://gitlab.com/geocastbrasil/liver/-/issues

---

class: inverse, middle, center

# 1 Desafio da We R Live 06

---

# 1 Desafio da We R Live 06

### Extrair dados climáticos para pontos
- WorldClim e Data Paper

--

### Extras: 
- Mapas raster usando o **`ggplot2`**
- Atlantic Series: dados de biodiversidade para a Mata Atlântica
- Converter uma tabela de coordenadas para vetor
- Extração de valores dos rasters para pontos
- Estatísticas simples
- tidyverse

--

background-image: url(img/map_atlantic.png),url(img/map_bio01_hist.png)
background-size: 250px,350px
background-position: 45% 95%,95% 95%

---

class: inverse, middle, center

# 2 Pacotes a serem usados

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 85% 95%

# 2 Pacotes a serem usados

### **`sf`**
https://r-spatial.github.io/sf/index.html

### **`raster`**
https://rspatial.org/raster/

### **`tidyverse`** - tibble, tidyr, dplyr, ggplot2
https://www.tidyverse.org/

### **`cptcity`**
https://ibarraespinosa.github.io/cptcity/

### **`patchwork`**
https://patchwork.data-imaginist.com/

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 50% 95%

# 2 Pacotes a serem usados

## Instalação

### Instalar pacotes
```{r eval=FALSE}
install.packages(c("sf", "raster", "tidyverse", "cptcity", "patchwork"), 
                 dependencies = TRUE)
```

---

class: inverse, middle, center

# 3 Considerações conceituais

---

class: inverse, middle, center

# Dados climáticos

---

# 3 Considerações conceituais

## O que é o WorldClim?

- Principal bases de **dados climáticos** para o mundo

--

- Dados temporais de temperatura e precipitação

--

- Construído à partir de dados de estações meteorológicas, interpolação e topografia

--

- Principal forma de uso: variáveis bioclimáticas

--

background-image: url(./img/wc_artigo.png)
background-size: 600px
background-position: 50% 80%

<br><br><br><br><br><br><br>

[https://doi.org/10.1002/joc.5086](https://doi.org/10.1002/joc.5086)

---

background-image: url(./img/wc_mapas.png)
background-size: 450px
background-position: 50% 75%

# 3 Considerações conceituais

## Estações meteorológicas

---

background-image: url(./img/wc_method.png)
background-size: 600px
background-position: 50% 60%

# 3 Considerações conceituais

## Interpolação

---

background-image: url(./img/wc_interpolation.png)
background-size: 500px
background-position: 50% 65%

# 3 Considerações conceituais

## Interpolação

---

# 3 Considerações conceituais

## Variáveis Bioclimáticas

- São 19 variáveis relacionadas com a biologia das organismos

--

- Combinações temporais de variáveis de temperatura (BIO01-BIO11) e precipitação (BIO12-BIO19)

--

background-image: url(./img/bioclim.png)
background-size: 300px
background-position: 50% 75%

---

background-image: url(./img/bio01.png)
background-size: 800px
background-position: 50% 80%

# 3 Considerações conceituais

## Variáveis Bioclimáticas

- BIO01: Temperatura média anual (º C)

---

background-image: url(./img/merraclim.jpg)
background-size: 700px
background-position: 50% 65%

# 3 Considerações conceituais

## Variáveis Bioclimáticas

- Disponível em várias resoluções e períodos (passado e futuro)

---

background-image: url(./img/worldclim.png)
background-size: 900px
background-position: 50% 65%

# 3 Considerações conceituais

## Site

[WorldClim](https://www.worldclim.org/)

---

class: inverse, middle, center

# Dados de biodiversidade

---

background-image: url(img/ats.jpg),url(img/goncalves-2018-fig2.png)
background-size: 200px,500px
background-position: 90% 90%,20% 90%

# 3 Considerações conceituais

### Atlantic Series: dados de biodiversidade para a Mata Atlântica

- Conjuntos de dados de comunidades de animais, plantas e processos ecológicos

- Localização: Bioma da Mata Atlântica

---

background-image: url(img/ats.jpg)
background-size: 300px
background-position: 75% 80%

# 3 Considerações conceituais

### Atlantic Series: dados de biodiversidade para a Mata Atlântica

- Frugivoria
- Traços funcionais
- Mamíferos
- Aves
- Anfíbios
- Morcegos
- Primatas
- Borboletas
- Epífitas
- ...

<a href="https://esajournals.onlinelibrary.wiley.com/doi/toc/10.1002/(ISSN)1939-9170.AtlanticPapers">Ecology</a>

[GitHub](https://github.com/LEEClab/Atlantic_series)

---

background-image: url(img/atlantic_mammals_artigo.png),url(img/atlantic_mammals.png),url(img/yuri.jpg)
background-size: 800px,200px,300px
background-position: 25% 7%,20% 75%,80% 70%

# 3 Considerações conceituais

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

DOI: [https://doi.org/10.1002/ecy.2785](https://doi.org/10.1002/ecy.2785)

---

background-image: url(img/coord_to_vector.png)
background-size: 800px
background-position: 50% 55%

# 3 Considerações conceituais

## Coordenadas para vetor

---

background-image: url(img/extraction_multi.jpg)
background-size: 600px
background-position: 50% 55%

# 3 Considerações conceituais

## Extração de valores dos rasters para pontos

---

background-image: url(img/graficos.jpg)
background-size: 450px
background-position: 50% 60%

# 3 Considerações conceituais

## Estatísticas

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [Facebook - Memes de Ciência](https://www.facebook.com/groups/cienciamemes/)

---

class: inverse, middle, center

# Mas antes: vocês têm 10 minutos para a palavra do tidyverse?

---

class: clear, center, middle
background-image: url(img/package_tidyverse.png)
background-size: 400px
background-position: 50% 50%

---

# tidyverse

### O tidyverse é um **conjunto de pacotes** designados para **Data Science** (mas não só)

<br>

--

### Todos os pacotes compartilham uma **filosofia** de design, gramática e estruturas de dados

<br>

--

### É um **"dialeto"** novo para a linguagem R

<br>

--

### **tidy**: organizado, arrumado, ordenado

### **verse**: universo

---

class: inverse, center
background-image: url(img/gif_avenger_reference.gif)
background-size: 800px

# Iniciativa Vingadores do R

---

background-image: url(img/tidyverse_flowchart.png)
background-size: 800px
background-position: 50% 60%

# tidyverse

---

background-image: url(img/person_hadley_wickham.jpg)
background-size: 550px
background-position: 50% 60%

# tidyverse

### O idealizador foi o **Hadley Wickham** e atualmente **muitas pessoas** têm contribuído para sua expansão

<br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] http://hadley.nz/

---

background-image: url(img/tidyverse_article_tidy.png)
background-size: 500px
background-position: 50% 55%

# tidyverse

### Tidy Data (2014) - *Journal of Statistics Software*

#### Hadley Wickham

<br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] http://vita.had.co.nz/papers/tidy-data.pdf

---

background-image: url(img/cover_data_science_r.png)
background-size: 300px
background-position: 50% 55%

# tidyverse

## R for Data Science (2017)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://r4ds.had.co.nz/

---

background-image: url(img/tidyverse_site1.png)
background-size: 800px
background-position: 50% 50%

# tidyverse

## Site

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

[*] https://www.tidyverse.org/

---

# tidyverse

### Para utilizar os pacotes do **tidyverse** é preciso instalar e carregar o pacote `tidyverse`

```{r,eval=FALSE}
# instalar o pacote
install.packages("tidyverse")
```

--

```{r,message=TRUE}
# carregar o pacote
library(tidyverse)
```

---

background-image: url(img/code_snake_case.png)
background-size: 500px
background-position: 80% 80%

# tidyverse

## IMPORTANTE!

### Todas as funções dos pacotes atrelados ao **tidyverse** usam `_` (underscore) para separar os nomes internos das funções (snake_code)

<br><br>

`read_csv()`

`read_xlsx()`

`as_tibble()`

`left_join()`

`group_by()`

---

# tidyverse

## Listar todos os pacotes do tidyverse

<br><br>

```{r}
# list all packages in the tidyverse 
tidyverse::tidyverse_packages(include_self = TRUE)
```

---

class: clear, center, middle
background-image: url(img/package_magrittr.png)
background-size: 400px
background-position: 50% 50%

---

background-image: url(img/person_rene_magritte.jpg), url(img/tidyverse_magritte_pipe.jpg)
background-size: 250px, 500px
background-position: 10% 30%, 85% 75%

# magrittr (pipe - %>%)

## René Magritte (1898-1967)

---

# magrittr (pipe - %>%)

### O operador pipe (*%>%*) permite o “encadeamento” de várias funções e **não é preciso de objetos** para armazenar resultados intermediários

--

### Essa função torna os códigos em R **mais simples**, pois realizamos **múltiplas operações** em uma **única linha**

--

### Ele captura o **resultado de uma declaração** e o **torna a entrada da próxima declaração**. Podemos pensar como *“EM SEGUIDA FAÇA”*

--

### O operador pipe é **%>%**

---

# magrittr (pipe - %>%)

### Atalho: `crtl + shift + M`
```{r}
# sem pipe
sqrt(sum(1:100))
```

--

```{r}
# com pipe
1:100 %>% 
  sum() %>% 
  sqrt()
```

---

class: clear, inverse, center, middle
background-image: url(img/gif_mario.gif)
background-size: 700px
background-position: 50% 50%

---

class: clear, center, middle
background-image: url(img/package_readr.png)
background-size: 400px
background-position: 50% 50%

---

# readr

### **Carrega e salva** grandes arquivos de forma **mais rápida**

--

### As funções **read.csv()** e **read.csv2()** são substituídas pelas funções **read_csv()** e **read_csv2()**

--

### Essas funções fornecem **medidores de progresso**

--

### E também **classificam** automaticamente o **modo** dos dados de cada coluna

--

### A classe do objeto atribuído é **tibble**

--

### Para salvar arquivos no formato .csv: **write_csv()** e **write_csv2()**

---

class: clear, center, middle
background-image: url(img/package_tibble.png)
background-size: 400px
background-position: 50% 50%

---

# 3.5 tibble

<br>

### O tibble (classe *tbl_df*) é um **tipo especial de data frame**

<br>

--

### É o **formato** aconselhado para que as funções do tidyverse **funcionem**

<br>

--

### Cada variável pode ser do tipo *numbers(int, dbl)*, *character(chr)*, *logical(lgl)* ou *factor(fctr)*

---

# 3.5 tibble

## Descrição dos modos das colunas através da função `glimpse()` - "espiar os dados"

```{r, include = FALSE}
si <- readr::read_csv("/home/mude/data/github/minicurso-tidyverse/03_dados/ATLANTIC_AMPHIBIANS_sites.csv")
```
--
```{r}
tibble::glimpse(si)
```

---

class: clear, center, middle
background-image: url(img/package_tidyr.png)
background-size: 400px
background-position: 50% 50%

---

# 3.6 tidyr

### Os conjuntos de dados **tidy** (organizados) são fáceis de manipular, modelar e visualizar

--

### Um conjunto de dados está **arrumado ou não**, dependendo de como linhas, colunas e células são combinadas com observações, variáveis e valores

--

## Nos dados tidy:
### 1 Cada variável em uma coluna
### 2 Cada observação em uma linha
### 3 Cada valor como uma célula

---

background-image: url(img/tidyr_data.png)
background-size: 800px
background-position: 50% 50%

# 3.6 tidyr

---

# 3.6 tidyr

## Funções

#### **1 unite()**: junta dados de múltiplas colunas em uma

#### **2 separate()**: separa caracteres em múlplica colunas

#### **3 separate_rows()**: separa caracteres em múlplica colunas e linhas

#### **4 drop_na()**: retira linhas com NA

#### **5 replace_na()**: substitui NA

#### **6 spread() => pivot_wider()**: long para wide

#### **7 gather() => pivot_longer()**: wide para long

---

class: clear, center, middle
background-image: url(img/package_dplyr.png)
background-size: 400px
background-position: 50% 50%

---

# 3.7 dplyr

### O **dplyr** é um pacote que **facilita** o trabalho com dados, com uma **gramática de manipulação** de dados **simples e flexível** (filtragem, reordenamento, seleção, entre outras)

--

### Ele foi construído com o intuito de obter uma forma **mais rápida** e **expressiva** de tratar os dados 

--

### O **tibble** é a **versão de data frame** mais **conveniente** para **se usar** com dplyr

--

### Sua gramática simples contém **funções verbais** para manipulação de dados

---

# 3.7 dplyr

### Sua gramática simples contém **funções verbais** para manipulação de dados

1. select(): seleciona colunas pelo nome gerando um tibble<br>
2. pull(): seleciona uma coluna como vetor<br>
3. rename(): muda o nome das colunas<br>
4. mutate(): adiciona novas colunas ou adiciona resultados em colunas existentes<br>
5. arrange(): reordenar as linhas com base nos valores de colunas<br>
6. filter(): seleciona linhas com base em valores<br>
7. distinc(): remove linhas com valores repetidos com base nos valores de colunas<br>
8. slice(): seleciona linhas pelos números<br>
9. sample_n(): amostragem aleatória de linhas<br>
10. summarise(): agrega ou resume os dados através de funções, podendo considerar valores das colunas<br>
11. *_join(): junta dados de duas tabelas através de uma coluna chave

---

class: inverse, middle, center

# Mas onde posso aprender mais sobre tidyverse?

--

<br>

# [site](https://www.ramambiental.com.br/cursos)

---

class: inverse, middle, center

# Mão na massa!!! `r icon::fa_laptop_code()`

---

# Script para essa live

<br><br><br><br><br><br><br>

### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive06/werlive06.R`]

---

class: inverse, middle, center

# 6 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

---

background-image: url(img/webinarlabgis_Prancheta-1-800x445.jpg)
background-size: 600px
background-position: 50% 85%

# 6 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

## Próxima Live: 

### We R Live 07: Introdução à estatística espacial: Análise de processos pontuais

---

class: inverse, center
background-image: url(img/miguel.jpeg)
background-size: 600px

MIGUEL OTÁVIO SANTANA DA SILVA, 5 anos <br>
foi impedido pela empregadora de sua <br> mãe de ficar junto dela e caiu do 9º andar

---

class: clear
background-image: url(img/logo_werlive.png),url(img/antifa.jpg)
background-size: 400px,100px
background-position: 90% 60%,95% 5%

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [@mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)