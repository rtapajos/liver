#' ---
#' title: We R Live 06: Extraindo dados climáticos para pontos
#' author: mauricio vancine
#' date: 2020-06-09
#' ---

# prepare r -------------------------------------------------------------
# packages
library(raster) # Geographic Data Analysis and Modeling, CRAN v3.1-5 
library(sf) # Simple Features for R, CRAN v0.9-4 
library(tidyverse) # Easily Install and Load the 'Tidyverse', CRAN v1.3.0 
library(cptcity) # 'cpt-city' Colour Gradients, CRAN v1.0.4 
library(patchwork) # The Composer of Plots, CRAN v1.0.1 

# directory
setwd("data")
getwd()

# import data -------------------------------------------------------------
# worldclim
# import
var <- dir(pattern = ".tif$") %>% 
  raster::stack()
var

# map
plot(var$bio01)

# map
var$bio01

var$bio01 %>% # selecionei a bio01
  raster::rasterToPoints() %>% # converti para pontos (lon, lat, val)
  head() # retorno 6 linhas

var$bio01 %>% 
  raster::rasterToPoints() %>% 
  tibble::as_tibble()

map_raster_bio01 <- var$bio01 %>% 
  raster::rasterToPoints() %>% 
  tibble::as_tibble() %>% 
  ggplot() + 
  aes(x = x, y = y, fill = bio01) +
  geom_raster() +
  scale_fill_viridis_c() +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", fill = "BIO01") +
  theme(legend.position = c(.8, .2))
map_raster_bio01

# atlantic forest
af <- sf::st_read("mata_atlantica.shp")
af

map_af <- ggplot() + 
  geom_sf(data = af) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude")
map_af

# data paper
da <- readr::read_csv("atlantic_mammals.csv")
da

# convert coordinates do vector -------------------------------------------
# convert
da_vector <- da %>% 
  sf::st_as_sf(coords = c("Longitude", "Latitude"), crs = 4326) # epsg.io
da_vector

# export
sf::st_write(da_vector, "atlantic_mammals.shp")
sf::st_write(da_vector, "atlantic_mammals.gpkg", layer = "atlantic_mammals2")

# map
map_nsp <- ggplot() + 
  geom_sf(data = af) +
  geom_sf(data = da_vector, aes(size = n_species), color = "black", 
          fill = "purple", pch = 21, alpha = .5) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", size = "Número de espécies") +
  theme(legend.position = c(.8, .2))
map_nsp

# extract values from raster to points ------------------------------------
# extract values
da_vector_bio <- raster::extract(var, da_vector)
da_vector_bio
head(da_vector_bio)

# extract values and bind cols
da_vector_bio <- raster::extract(var, da_vector) %>% 
  tibble::as_tibble() %>% 
  dplyr::bind_cols(da_vector, .)
da_vector_bio

# map bio01
map_bio01 <- ggplot() + 
  geom_sf(data = af) +
  geom_sf(data = da_vector_bio, aes(fill = bio01), shape = 21, size = 4, alpha = .5) +
  scale_fill_gradientn(colors = cptcity::cpt(pal = cptcity::find_cpt("temperature")[2], n = 30)) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", size = "Número de espécies") +
  theme(legend.position = c(.8, .2))
map_bio01

# map bio12
map_bio12 <- ggplot() + 
  geom_sf(data = af) +
  geom_sf(data = da_vector_bio, aes(fill = bio12), shape = 21, size = 4, alpha = .5) +
  scale_fill_gradientn(colors = cptcity::cpt(pal = cptcity::find_cpt("precipitation")[2], n = 30)) +
  coord_sf() +
  theme_minimal() +
  labs(x = "longitude", y = "latitude", size = "Número de espécies") +
  theme(legend.position = c(.8, .2))
map_bio12

# statistics --------------------------------------------------------------
# histogram bio01
hist_bio01 <- da_vector_bio %>% 
  ggplot() +
  aes(x = bio01) +
  geom_histogram(color = "white", bins = 15, fill = cptcity::cpt(pal = cptcity::find_cpt("temperature")[2], n = 15)) +
  theme_minimal() +
  labs(x = "BIO01", y = "Frequência Absoluta")
hist_bio01

# histogram bio12
hist_bio12 <- da_vector_bio %>% 
  ggplot() +
  aes(x = bio12) +
  geom_histogram(color = "white", bins = 30, fill = cptcity::cpt(pal = cptcity::find_cpt("precipitation")[2], n = 30)) +
  theme_minimal() +
  labs(x = "BIO12", y = "Frequência Absoluta")
hist_bio12

# combine - patchwork
map_bio01 + hist_bio01
map_bio12 + hist_bio12

(map_bio01 / hist_bio01) | (map_bio12 / hist_bio12)
(map_bio01 | hist_bio01) / (map_bio12 | hist_bio12)

# histograms with facet ---------------------------------------------------
# wide to long
da_vector_bio_long <- da_vector_bio %>% 
  sf::st_drop_geometry() %>% 
  dplyr::select(ID, contains("bio0"), contains("bio1")) %>% 
  tidyr::pivot_longer(cols = -ID, names_to = "bios", values_to = "values")
da_vector_bio_long

# histogram
hist_facet <- da_vector_bio_long %>% 
  ggplot() +
  aes(x = values) +
  geom_histogram(color = "white", bins = 30, 
                 fill = c(rep(cptcity::cpt(pal = cptcity::find_cpt("temperature")[2], n = 30), 11),
                          rep(cptcity::cpt(pal = cptcity::find_cpt("precipitation")[2], n = 30), 8))) +
  facet_wrap(vars(bios), scales = "free") +
  theme_bw() +
  labs(x = "Valores", y = "Frequência Absoluta")
hist_facet

# end ---------------------------------------------------------------------

