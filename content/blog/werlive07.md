---
date: "2020-06-16T22:00-23:00"
tags:
- r
- rspatial
- processo pontual
- spatstat
title: We R Live 07 - Introdução à Estatística Espacial I
---

{{<youtube fHWD4qyKj84>}}

Enfim daremos início a uma série de lives sobre estatistica espacial.

Para começar, vamos falar sobre Processos Pontuais, como se costuma chamar os fenômenos mapeados como pontos.

Um dos tipos mais simples de dados geográficos e ainda ssim, muitas vezes menosprezado. Vamos aos poucos destrinchando conceitos para facilitar seu entendimento e para que você possa aprofundar no assunto sem tanto sofrimento.

Slides: https://werlive.netlify.app/werlive07/werlive07

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive07/werlive07.R

Vídeo: https://youtu.be/fHWD4qyKj84