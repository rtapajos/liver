---
date: "2020-06-02T22:00-23:00"
tags:
- r
- rspatial
- sensoriamentoremoto
- ndvi
title: We R Live 05 - Manipulando dados raster no R II
---

{{<youtube AKJo_Q0dsMI>}}

Chegou a hora de complexificar um pouco. Nesta live vamos trabalhar com dados raster com mais de uma camada. Um grande exemplo: imagens de satélite.

Veremos como carregar, quais a diferenças entre as classes RasterStack e RasterBrick, como calcular índices espectrais e muitos truques para manipular este tipo de dado.

Slides: https://werlive.netlify.app/werlive05/werlive05

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive05/werlive05.R

Vídeo: https://youtu.be/AKJo_Q0dsMI