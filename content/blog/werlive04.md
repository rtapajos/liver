---
date: "2020-05-26T22:00-23:00"
tags:
- r
- rspatial
- raster
title: We R Live 04 - Manipulando dados raster no R
---

{{<youtube dFC9SuGLuX8>}}

Dando seguimento às lives de R, vamos iniciar a trabalhar com dados no formato 'raster'. Começaremos do basicão, mas será o suficiente para dominar a manipulação deste tipo de dado espacial tão usado.

Slides: https://werlive.netlify.app/werlive04/werlive04

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive04/werlive04.R

Vídeo: https://youtu.be/dFC9SuGLuX8