---
date: "2020-06-09T22:00-23:00"
tags:
- r
- rspatial
- raster
- bioclim
title: We R Live 06 - Extraindo dados climáticos para pontos
---

{{<youtube -_ODMFDU6ck>}}

Para fechar o assunto de raster (pelo menos por enquanto:). 

Vamos aprender como converter dados tabulares em um vetor de pontos, como extrair valores dos rasters para pontos e por fim fechamos com a visualização de tais dados em forma de gráficos.

Tudo isso com dados do WorldClim e de um Data Paper de Mamíferos.

Slides: https://werlive.netlify.app/werlive06/werlive06

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive06/werlive06.R

Vídeo: https://youtu.be/-_ODMFDU6ck