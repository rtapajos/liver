---
date: "2020-06-30T22:00-23:00"
tags:
- r
- rspatial
- processo pontual
- spatstat
title: We R Live 09 - Introdução à Estatística Espacial III
---

{{<youtube eaR7pTsQFDQ>}}

Para fechar essa série inicial de introdução à estatística espacial, onde apresentamos análises de processos pontuais, vamos analizar se os tiroteios ocorridos em 2019, na cidade do Rio de Janeiro, tiveram a tendencia de ocorrer próximo às escolas.

Para isso, fecharemos o assunto apresentando a análise de processos pontuais bivariada. 

Slides: https://werlive.netlify.app/werlive09/werlive09

Script: https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive09/werlive09.R

Vídeo: https://youtu.be/eaR7pTsQFDQ